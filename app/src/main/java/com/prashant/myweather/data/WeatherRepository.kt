package com.prashant.myweather.data

import com.prashant.myweather.model.WeatherData
import com.prashant.myweather.network.WebService
import retrofit2.Callback

class WeatherRepository private constructor(private val webService: WebService) {

    fun getWeatherDetail(lat: Double, long: Double, appId: String, unit: String?, callback: Callback<WeatherData>) {
        val call = webService.getCurrentWeather(lat, long, appId, unit)
        call.enqueue(callback)
    }

    companion object {

        // For Singleton instantiation
        @Volatile
        private var instance: WeatherRepository? = null

        fun getInstance(webService: WebService) =
            instance ?: synchronized(this) {
                instance ?: WeatherRepository(webService).also { instance = it }
            }
    }
}