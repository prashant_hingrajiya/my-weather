package com.prashant.myweather.data

import com.google.samples.apps.sunflower.utilities.runOnIoThread


/**
 * Repository module for handling data operations.
 */
class LocationRepository private constructor(private val locationDao: LocationDao) {

    fun getLocations() = locationDao.getLocations()

    fun getLocationById(locationId: String) = locationDao.getLocation(locationId)
    fun saveLocation(location: Location) {
        runOnIoThread {
            locationDao.insertLocation(location)
        }
    }

    fun deleteLocation(locationId: String) {
        runOnIoThread {
            locationDao.deleteLocation(locationId)
        }
    }

    companion object {

        // For Singleton instantiation
        @Volatile
        private var instance: LocationRepository? = null

        fun getInstance(plantDao: LocationDao) =
            instance ?: synchronized(this) {
                instance ?: LocationRepository(plantDao).also { instance = it }
            }
    }
}
