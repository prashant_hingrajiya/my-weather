package com.prashant.myweather.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * The Data Access Object for the Plant class.
 */
@Dao
interface LocationDao {
    @Query("SELECT * FROM locations ORDER BY name")
    fun getLocations(): LiveData<List<Location>>

    @Query("SELECT * FROM locations WHERE id = :locationId")
    fun getLocation(locationId: String): LiveData<Location>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(plants: List<Location>)

    @Insert
    fun insertLocation(location: Location): Long

    @Query("DELETE FROM locations WHERE id = :locationId")
    fun deleteLocation(locationId: String)
}
