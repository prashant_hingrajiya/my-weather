package com.prashant.myweather.data

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "locations")
data class Location(
    @PrimaryKey @ColumnInfo(name = "id") @NonNull var locationId: String = "",
    var name: String? = "",
    var address: String? = "",
    var lat: Double? = 0.toDouble(),
    var lon: Double? = 0.toDouble()
)
