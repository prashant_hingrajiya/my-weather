/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.prashant.myweather.network

import com.google.gson.Gson
import com.prashant.myweather.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ServiceProvider {

    companion object {
        val APP_ID = "c6e381d8c7ff98f0fee43775817cf6ad"
        private var service: WebService? = null
        fun apiService(): WebService {
            if (service == null) {
                val mBaseUrl = BuildConfig.BASE_URL
                val interceptor = HttpLoggingInterceptor()

                interceptor.level =
                        if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
                val client = OkHttpClient.Builder().addInterceptor(interceptor)
                    .build()

                service = Retrofit.Builder().baseUrl(mBaseUrl)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(Gson()))
                    .build().create(WebService::class.java)
            }
            return service!!

        }
    }
}