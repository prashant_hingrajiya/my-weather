package com.prashant.myweather.ui.locationlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.prashant.myweather.R
import com.prashant.myweather.databinding.WeatherDetailFragmentBinding
import com.prashant.myweather.network.ServiceProvider
import com.prashant.myweather.utilities.InjectorUtils

class WeatherDetailFragment : Fragment() {

    companion object {
        val LATITUDE = "lat"
        val LONGITUDE = "long"
        fun newInstance(lat: Double, long: Double): WeatherDetailFragment {
            val bundle = Bundle()
            bundle.putDouble(LATITUDE, lat)
            bundle.putDouble(LONGITUDE, long)
            val fragment = WeatherDetailFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    private lateinit var viewModel: WeatherDetailViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val factory = InjectorUtils.provideWeatherDetailViewModelFactory()
        viewModel = ViewModelProviders.of(this, factory)
            .get(WeatherDetailViewModel::class.java)

        val binding = DataBindingUtil.inflate<WeatherDetailFragmentBinding>(
            inflater, R.layout.weather_detail_fragment, container, false
        ).apply {

            weatherViewModel = viewModel
            setLifecycleOwner(this@WeatherDetailFragment)

        }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val latitude = arguments?.getDouble(LATITUDE)
        val longitude = arguments?.getDouble(LONGITUDE)

        viewModel.fetchCurrentWeather(latitude!!, longitude!!, ServiceProvider.APP_ID, null)

    }
}
