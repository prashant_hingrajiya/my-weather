package com.prashant.myweather.ui.locationlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

import com.prashant.myweather.data.LocationRepository

/**
 * Factory for creating a [PlantListViewModel] with a constructor that takes a [PlantRepository].
 */
class LocationListViewModelFactory(
    private val repository: LocationRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>) = LocationListViewModel(repository) as T
}
