package com.prashant.myweather.ui.locationlist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.prashant.myweather.data.WeatherRepository
import com.prashant.myweather.model.WeatherData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WeatherDetailViewModel(
    private val weatherRepository: WeatherRepository
) : ViewModel() {

    var weatherData: WeatherData? = null
    val cityName = MutableLiveData<String>()
    val weather = MutableLiveData<String>()
    val temperature = MutableLiveData<String>()
    val humidity = MutableLiveData<String>()
    val rain = MutableLiveData<String>()
    val wind = MutableLiveData<String>()
    val image = MutableLiveData<String>()

    fun fetchCurrentWeather(lat: Double, long: Double, appId: String, unit: String?) {
        weatherRepository.getWeatherDetail(lat, long, appId, unit, object : Callback<WeatherData> {
            override fun onFailure(call: Call<WeatherData>, t: Throwable) {
                t.printStackTrace()
            }

            override fun onResponse(call: Call<WeatherData>, response: Response<WeatherData>) {
                if (response.isSuccessful) {
                    weatherData = response.body()
                    cityName.postValue(weatherData?.name)
                    weather.postValue(weatherData?.weather!![0].main + ", " + weatherData?.weather!![0].description)
                    temperature.postValue("Temperature: " + weatherData?.main?.temp.toString() + " k")
                    humidity.postValue("Humidity: " + weatherData?.main?.humidity.toString())
                    rain.postValue("Rain: " + if (weatherData?.rain?.rain.isNullOrBlank()) "No" else weatherData?.rain?.rain)
                    wind.postValue("Wind: " + weatherData?.wind?.speed.toString())
                    image.postValue("http://openweathermap.org/img/w/${weatherData?.weather!![0].icon}.png")
                }
            }

        })
    }
}
