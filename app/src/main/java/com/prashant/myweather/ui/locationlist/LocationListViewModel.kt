package com.prashant.myweather.ui.locationlist

import androidx.lifecycle.ViewModel
import com.prashant.myweather.data.Location
import com.prashant.myweather.data.LocationRepository


class LocationListViewModel internal constructor(
    private val plantRepository: LocationRepository
) : ViewModel() {

    fun pinThisLocation(location: Location) {
        plantRepository.saveLocation(location)
    }

    fun removeLocation(locationId: String) {
        plantRepository.deleteLocation(locationId)
    }

    val locations = plantRepository.getLocations()
}
