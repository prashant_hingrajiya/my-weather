package com.prashant.myweather.ui.locationlist

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.location.places.ui.PlacePicker
import com.prashant.myweather.WeatherActivity
import com.prashant.myweather.adapters.LocationAdapter
import com.prashant.myweather.data.Location
import com.prashant.myweather.databinding.LocationListFragmentBinding
import com.prashant.myweather.utilities.InjectorUtils


class LocationListFragment : Fragment() {
    val PLACE_PICKER_REQUEST = 1
    //    val PERMISSION_REQUEST_CODE = 100
    lateinit var viewModel: LocationListViewModel

    companion object {
        fun newInstance() = LocationListFragment()
    }

    val clickListener = object : LocationAdapter.ListClickListner {
        override fun onLongClick(locationId: String): Boolean {
            removeLocation(locationId)
            return true
        }

        override fun onClick(latitude: Double, longitude: Double) {
            openDetailFragment(latitude, longitude)
        }

    }

    private fun openDetailFragment(latitude: Double, longitude: Double) {
        (activity as WeatherActivity).openWeatherDetail(latitude, longitude)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = LocationListFragmentBinding.inflate(inflater, container, false)
        val adapter = LocationAdapter()
        adapter.clickListener = clickListener
        binding.locationList.adapter = adapter
        subscribeUi(adapter, binding)
        return binding.root
    }

    private fun subscribeUi(adapter: LocationAdapter, binding: LocationListFragmentBinding) {
        val factory = InjectorUtils.provideLocationListViewModelFactory(requireContext())
        viewModel = ViewModelProviders.of(this, factory)
            .get(LocationListViewModel::class.java)

        viewModel.locations.observe(viewLifecycleOwner, Observer { locationList ->
            val isListNotEmpty = locationList != null && locationList.isNotEmpty()
            binding.hasLocations = isListNotEmpty
            if (isListNotEmpty)
                adapter.submitList(locationList)
        })

        binding.clickListener = View.OnClickListener {
            val builder = PlacePicker.IntentBuilder()
            try {
                startActivityForResult(builder.build(this.activity), PLACE_PICKER_REQUEST)
            } catch (e: GooglePlayServicesRepairableException) {
                e.printStackTrace()
            } catch (e: GooglePlayServicesNotAvailableException) {
                e.printStackTrace()
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode === PLACE_PICKER_REQUEST) {
            if (resultCode === Activity.RESULT_OK) {
                val place = PlacePicker.getPlace(context!!, data!!)
                val location = Location(
                    place.id,
                    place.name.toString(),
                    place.address.toString(),
                    place.latLng.latitude,
                    place.latLng.longitude
                )
                showPinAlert(location)

            }
        }
    }

    private fun showPinAlert(location: Location) {
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle("Pin Location")
            .setMessage("Do you want to pin this location?")
            .setPositiveButton(android.R.string.yes) { dialog, which ->
                dialog.dismiss()
                viewModel.pinThisLocation(location)
                openDetailFragment(location.lat!!, location.lon!!)
                Toast.makeText(context, "Long press on Location item to remove", Toast.LENGTH_LONG).show()
            }
            .setNegativeButton(android.R.string.no) { dialog, which ->
                dialog.dismiss()
                openDetailFragment(location.lat!!, location.lon!!)
            }
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show()
    }

    private fun removeLocation(locationId: String) {
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle("Remove Location")
            .setMessage("Do you want to remove this location?")
            .setPositiveButton(android.R.string.yes) { dialog, which ->
                dialog.dismiss()
                viewModel.removeLocation(locationId)

            }
            .setNegativeButton(android.R.string.no) { dialog, which ->
                dialog.dismiss()
            }
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show()
    }
}
