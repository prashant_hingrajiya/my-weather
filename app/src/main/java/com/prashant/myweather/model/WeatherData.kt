package com.prashant.myweather.model

class WeatherData {
    var weather: List<Weather>? = null
    var main: Data? = null
    var wind: Wind? = null
    var rain: Rain? = null
    var snow: Snow? = null
    var name: String? = null
}
