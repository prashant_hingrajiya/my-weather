package com.prashant.myweather.model

import com.google.gson.annotations.SerializedName

class Rain {
    @SerializedName("3h")
    var rain: String? = null
}
