package com.prashant.myweather.model

import com.google.gson.annotations.SerializedName

class Data {
    var temp: Double? = null
    var pressure: Double? = null
    var humidity: Int? = null
    @SerializedName("temp_min")
    var tempMin: Double? = null
    @SerializedName("temp_max")
    var tempMax: Double? = null
    @SerializedName("sea_level")
    var seaLevel: Double? = null
    @SerializedName("grnd_level")
    var grndLevel: Double? = null
}
