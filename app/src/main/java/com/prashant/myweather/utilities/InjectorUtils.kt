package com.prashant.myweather.utilities

import android.content.Context
import com.prashant.myweather.data.AppDatabase
import com.prashant.myweather.data.LocationRepository
import com.prashant.myweather.data.WeatherRepository
import com.prashant.myweather.network.ServiceProvider
import com.prashant.myweather.ui.locationlist.LocationListViewModelFactory
import com.prashant.myweather.ui.locationlist.WeatherDetailViewModelFactory

/**
 * Static methods used to inject classes needed for various Activities and Fragments.
 */
object InjectorUtils {

    private fun getLocationRepository(context: Context): LocationRepository {
        return LocationRepository.getInstance(AppDatabase.getInstance(context).locationDao())
    }

    fun provideLocationListViewModelFactory(context: Context): LocationListViewModelFactory {
        val repository = getLocationRepository(context)
        return LocationListViewModelFactory(repository)
    }

    private fun getWeatherRepository(): WeatherRepository {
        return WeatherRepository.getInstance(
            ServiceProvider.apiService()
        )
    }

    fun provideWeatherDetailViewModelFactory(): WeatherDetailViewModelFactory {
        return WeatherDetailViewModelFactory(getWeatherRepository())
    }
}
