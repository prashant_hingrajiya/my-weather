package com.prashant.myweather

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.prashant.myweather.databinding.WeatherActivityBinding
import com.prashant.myweather.ui.locationlist.LocationListFragment
import com.prashant.myweather.ui.locationlist.WeatherDetailFragment

class WeatherActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: WeatherActivityBinding = DataBindingUtil.setContentView(
            this,
            R.layout.weather_activity
        )

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, LocationListFragment.newInstance())
                .commitNow()
        }
    }

    fun openWeatherDetail(latitude: Double, longitude: Double) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, WeatherDetailFragment.newInstance(latitude, longitude))
            .addToBackStack(WeatherDetailFragment.javaClass.simpleName)
            .commit()
    }

}
