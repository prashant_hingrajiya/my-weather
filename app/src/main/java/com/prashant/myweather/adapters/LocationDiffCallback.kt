package com.prashant.myweather.adapters

import androidx.recyclerview.widget.DiffUtil
import com.prashant.myweather.data.Location

class LocationDiffCallback : DiffUtil.ItemCallback<Location>() {

    override fun areItemsTheSame(oldItem: Location, newItem: Location): Boolean {
        return oldItem.locationId == newItem.locationId
    }

    override fun areContentsTheSame(oldItem: Location, newItem: Location): Boolean {
        return oldItem == newItem
    }
}