package com.prashant.myweather.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.prashant.myweather.data.Location
import com.prashant.myweather.databinding.ListItemLocationBinding

class LocationAdapter : ListAdapter<Location, LocationAdapter.ViewHolder>(LocationDiffCallback()) {
    lateinit var clickListener: ListClickListner
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val location = getItem(position)
        holder.apply {
            bind(
                createOnClickListener(location!!.lat!!, location.lon!!),
                createOnLongClickListener(location.locationId),
                location
            )
            itemView.tag = location
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ListItemLocationBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    private fun createOnClickListener(latitude: Double, longitude: Double): View.OnClickListener {
        return View.OnClickListener {
            clickListener.onClick(latitude, longitude)
        }
    }

    private fun createOnLongClickListener(locationId: String): View.OnLongClickListener {
        return View.OnLongClickListener {
            clickListener.onLongClick(locationId)
        }
    }

    class ViewHolder(
        internal val binding: ListItemLocationBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(listener: View.OnClickListener, longListener: View.OnLongClickListener, item: Location) {
            binding.apply {

                clickListener = listener
                longClickListener = longListener
                location = item
                executePendingBindings()
            }
        }
    }

    interface ListClickListner {
        fun onClick(latitude: Double, longitude: Double)
        fun onLongClick(locationId: String): Boolean
    }
}